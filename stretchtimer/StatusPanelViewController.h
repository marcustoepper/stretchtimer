//
//  StatusPanelViewController.h
//  stretchtimer
//
//  Created by Stefan Bayer on 23.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "GradientLabel.h"
#import "PositionView.h"


@interface StatusPanelViewController : UIViewController
{
    PositionView *positionView;
    UIView *setsView;
    UIView *exerciseView;
    
    UIImageView *positionImage;
    GradientLabel *setsLabel;
    GradientLabel *exerciseLabel;
    DataManager *dataManager;
}

@property (strong, nonatomic) PositionView *positionView;
@property (strong, nonatomic) UIView *setsView;
@property (strong, nonatomic) UIView *exerciseView;

@property (strong, nonatomic) UIImageView *positionImage;
@property (strong, nonatomic) GradientLabel *setsLabel;
@property (strong, nonatomic) GradientLabel *exerciseLabel;

@property (strong, nonatomic) DataManager *dataManager;

- (int)transformPositionPanelToStatus:(int)status;
- (void)transformSetsPanelWithSetsCount:(int)setsCount;
- (void)transformExercisePanelWithExerciseCount:(int)exerciceCount;

- (void)reset;

@end
