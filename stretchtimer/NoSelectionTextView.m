//
//  NoSelectionTextView.m
//  stretchtimer
//
//  Created by Alexander Schilling on 9/6/13.
//
//

#import "NoSelectionTextView.h"

@implementation NoSelectionTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(BOOL)canBecomeFirstResponder {
    return NO;
}

@end
