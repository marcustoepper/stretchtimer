//
//  DVPickerView.h
//  stretchtimer
//
//  Created by Alexander Schilling on 6/8/13.
//
//

#import <UIKit/UIKit.h>

//the width of the content to show (centered)
#define DV_PICKER_CONTENT_WIDTH 240
//the height of the highlight area
#define DV_PICKER_OVERLAY_HEIGHT 43
//the space between the top of the overlay and the bottom of the top border
#define DV_PICKER_OVERLAY_BORDER_SPACE 77
//border width
#define DV_PICKER_BORDER_WIDTH 10

//text color for unhighlighted rows
#define DV_PICKER_TEXT_COLOR_NORMAL [UIColor colorWithWhite:160.0/255.0 alpha:1.0]
//text color for highlighted rows
#define DV_PICKER_TEXT_COLOR_HIGHLIGHTED [UIColor colorWithPatternImage:[UIImage imageNamed:@"gradient.png"]]

//width mode definitions
typedef enum {
    DV_PICKER_WIDTH_MODE_EQUAL,
    DV_PICKER_WIDTH_MODE_EQUAL_CONDENSED,
    DV_PICKER_WIDTH_MODE_VARIABLE
} DV_PICKER_WIDTH_MODE;


//protocol forward definition
@protocol DVPickerViewDelegate;


@interface DVPickerView : UIView <UIScrollViewDelegate>

//delegate property
@property (strong, nonatomic) id <DVPickerViewDelegate> delegate;

//arrays for scroll views
@property (strong, nonatomic) NSMutableArray *backScrolls;
@property (strong, nonatomic) NSMutableArray *highlightScrolls;
@property (strong, nonatomic) NSMutableArray *actionScrolls;

//init method
//point: top left point of the picker
//components: Array of Arrays of Strings for the row labels in the components
//widthMode: either equal for equal component width or variable for dynamic component width
-(id)initAtPoint:(CGPoint)point withComponents:(NSArray *)components widthMode:(DV_PICKER_WIDTH_MODE)widthMode;

//getters and setters
-(int)getActiveRowForComponent:(int)component;
-(void)selectRow:(int)row inComponent:(int)component animated:(BOOL)animated;

@end


//protocol definition
@protocol DVPickerViewDelegate <NSObject>

//whenever a row is selected, this method gets called
-(void)pickerView:(DVPickerView *)pickerView didSelectRow:(int)row inComponent:(int)component;

@end
