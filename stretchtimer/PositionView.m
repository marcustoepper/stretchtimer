//
//  PositionView.m
//  stretchtimer
//
//  Created by Alexander Schilling on 5/3/13.
//
//

#import "PositionView.h"
#import "Definitions.h"

@implementation PositionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        animating = NO;
        [self setNeedsDisplay];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
    
    if (animating) {
        NSTimeInterval diff = [[NSDate date] timeIntervalSince1970]-animationStarted;
        if (diff > 5) animating = NO;
        CGContextSetFillColorWithColor(context, [UIColor colorWithRed:219.0/255.0 green:236.0/255.0 blue:242.0/255.0 alpha:pow(M_E,-0.5*diff)].CGColor);
        float radius = 47+diff*SCREEN_HEIGHT;
        CGContextFillEllipseInRect(context, CGRectMake(SCREEN_WIDTH/2-radius, 0.57*SCREEN_HEIGHT-radius, radius*2, radius*2));
    }
    
    if (animating) {
        [self performSelector:@selector(setNeedsDisplay) withObject:nil afterDelay:1.0/30.0];
    }
}


-(void)animate {
    animating = YES;
    animationStarted = [[NSDate date] timeIntervalSince1970];
    [self setNeedsDisplay];
}


@end
