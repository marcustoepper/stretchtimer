//
//  AppDelegate.h
//  stretchtimer
//
//  Created by Stefan Bayer on 23.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "DisclaimerViewController.h"
#import "SplashScreen.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UINavigationController *rootNavigationController;
    MainViewController *mainViewController;
    DisclaimerViewController *disclaimer;
    SplashScreen *splash;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *rootNavigationController;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) DisclaimerViewController *disclaimer;
@property (strong, nonatomic) SplashScreen *splash;

@end
