//
//  DisclaimerViewController.m
//  stretchtimer
//
//  Created by Alexander Schilling on 5/25/13.
//
//

#import "DisclaimerViewController.h"
#import "Definitions.h"
#import "NoSelectionTextView.h"

@interface DisclaimerViewController ()

@end

@implementation DisclaimerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UITextView *disclaimer = [[[NoSelectionTextView alloc] initWithFrame:CGRectMake(20, 0, 280, SCREEN_HEIGHT-90)] autorelease];
        [disclaimer setText:NSLocalizedString(@"DisclaimerKey", @"")];
        [disclaimer setBackgroundColor:[UIColor clearColor]];
        [disclaimer setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:11.0]];
        [disclaimer setEditable:NO];
        [disclaimer setTextColor:[UIColor colorWithWhite:100.0/255.0 alpha:1.0]];
        [disclaimer setShowsVerticalScrollIndicator:NO];
        [self.view addSubview:disclaimer];
        
        UILabel *terms = [[[UILabel alloc] initWithFrame:CGRectMake(0, 25, 280, 28)] autorelease];
        [terms setText:NSLocalizedString(@"TermsOfUseKey", @"")];
        [terms setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18.0]];
        [terms setBackgroundColor:[UIColor clearColor]];
        [terms setTextAlignment:NSTextAlignmentCenter];
        [terms setTextColor:[UIColor colorWithWhite:100.0/255.0 alpha:1.0]];
        [disclaimer addSubview:terms];
        
        UIImageView *topshadow = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 42)] autorelease];
        [topshadow setImage:[UIImage imageNamed:@"Shadow_top.png"]];
        [self.view addSubview:topshadow];
        
        UIImageView *bottomshadow = [[[UIImageView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-90-42, 320, 42)] autorelease];
        [bottomshadow setImage:[UIImage imageNamed:@"Shadow_bottom.png"]];
        [self.view addSubview:bottomshadow];
        
        UIButton *accept = [UIButton buttonWithType:UIButtonTypeCustom];
        [accept setFrame:CGRectMake(102, SCREEN_HEIGHT-58-20, 116, 45)];
        [accept setImage:[UIImage imageNamed:@"Button_Disclaimer.png"] forState:UIControlStateNormal];
        [accept addTarget:self action:@selector(accept) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:accept];
        
        UILabel *acceptLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 5, 116, 45)] autorelease];
        [acceptLabel setText:NSLocalizedString(@"AcceptKey", @"")];
        [acceptLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18.0]];
        [acceptLabel setBackgroundColor:[UIColor clearColor]];
        [acceptLabel setTextAlignment:NSTextAlignmentCenter];
        [acceptLabel setTextColor:[UIColor colorWithWhite:100.0/255.0 alpha:1.0]];
        [accept addSubview:acceptLabel];
    }
    return self;
}

-(void)accept {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DISCLAIMER_ACCEPTED"];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
