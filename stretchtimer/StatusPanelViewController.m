//
//  StatusPanelViewController.m
//  stretchtimer
//
//  Created by Stefan Bayer on 23.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StatusPanelViewController.h"
#import "Definitions.h"

@interface StatusPanelViewController ()

@end

@implementation StatusPanelViewController

@synthesize positionView;
@synthesize setsView;
@synthesize exerciseView;

@synthesize positionImage;
@synthesize setsLabel;
@synthesize exerciseLabel;

@synthesize dataManager;

- (void)dealloc
{
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.backgroundColor = [UIColor clearColor];
        
        self.dataManager = [[[DataManager alloc] init] autorelease];
        
        self.positionView = [[[PositionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
        [self.view addSubview:positionView];
        
        self.positionImage = [[[UIImageView alloc] initWithFrame:CGRectMake(110, 0.57*SCREEN_HEIGHT-50, 100, 100)] autorelease];
        [self.positionImage setImage:[UIImage imageNamed:@"Rest_Woman.png"]];
        [self.positionImage setContentMode:UIViewContentModeCenter];
        [self.view addSubview:self.positionImage];
        
        UIView *progressView = [[[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-50-80, SCREEN_WIDTH, 80)] autorelease];
        [progressView setBackgroundColor:[UIColor colorWithRed:97.0/255.0 green:172.0/255.0 blue:199.0/255.0 alpha:1.0]];
        [self.view addSubview:progressView];
        
        UIView *line = [[[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 20, 1, 40)] autorelease];
        [line setBackgroundColor:[UIColor whiteColor]];
        [progressView addSubview:line];
        
        self.exerciseLabel = [[[GradientLabel alloc] initWithFrame:CGRectMake(0, 17, SCREEN_WIDTH/2, 50)] autorelease];
        self.exerciseLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:46];
        self.exerciseLabel.text = [NSString stringWithFormat:@"0/%i", [self.dataManager getExerciseCount]];
        [progressView addSubview:self.exerciseLabel];
        
        self.setsLabel = [[[GradientLabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 17, SCREEN_WIDTH/2, 50)] autorelease];
        self.setsLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:46];
        self.setsLabel.text = @"0";
        [progressView addSubview:self.setsLabel];
        
        GradientLabel *exercisesLabel = [[[GradientLabel alloc] initWithFrame:CGRectMake(0, 55, SCREEN_WIDTH/2, 22)] autorelease];
        exercisesLabel.text = NSLocalizedString(@"Steps_Key", nil);
        [progressView addSubview:exercisesLabel];
        
        GradientLabel *setLabel = [[[GradientLabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2, 55, SCREEN_WIDTH/2, 22)] autorelease];
        setLabel.text = NSLocalizedString(@"Sets_Key", nil);
        [progressView addSubview:setLabel];
    }
    return self;
}

- (int)transformPositionPanelToStatus:(int)status
{
    [self.positionView animate];
    
    [UIView beginAnimations:nil context:nil];

    [UIView setAnimationDuration:1];

    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.positionImage cache:NO];
    
    [UIView commitAnimations];
    
    if (status == PAUSE) {
        [self.positionImage setImage:[UIImage imageNamed:@"Rest_Woman.png"]];
        return 1;
    } else {
        [self.positionImage setImage:[UIImage imageNamed:@"Stretch_Woman.png"]];
        return 0;
    }
}

- (void)transformSetsPanelWithSetsCount:(int)setsCount
{
    self.setsLabel.text = [NSString stringWithFormat:@"%i", setsCount];
}

- (void)transformExercisePanelWithExerciseCount:(int)exerciceCount
{
    self.exerciseLabel.text = [NSString stringWithFormat:@"%i/%i",exerciceCount,[self.dataManager getExerciseCount]];
}

- (void)reset
{
    [self transformPositionPanelToStatus:PAUSE];
    [self transformSetsPanelWithSetsCount:0];
    [self transformExercisePanelWithExerciseCount:0];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
