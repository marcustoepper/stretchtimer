//
//  SettingsViewController.h
//  stretchtimer
//
//  Created by Alexander Schilling on 5/4/13.
//
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "DVPickerView.h"
#import "ImprintViewController.h"

@interface SettingsViewController : UIViewController <DVPickerViewDelegate> {
    UIButton *settingsButton;
    
    UILabel *stepsLabel;
    
    UILabel *stretchTimeLabel;
    UILabel *pauseTimeLabel;
    
    DVPickerView *datePicker;
    
    DataManager *dataManager;
    
    int currentMode;
}

@property (strong, nonatomic) UIButton *settingsButton;

@property (strong, nonatomic) UILabel *stepsLabel;

@property (strong, nonatomic) UILabel *stretchTimeLabel;
@property (strong, nonatomic) UILabel *pauseTimeLabel;

@property (strong, nonatomic) DVPickerView *datePicker;

@property (strong, nonatomic) DataManager *dataManager;

@end
