//
//  AppDelegate.m
//  stretchtimer
//
//  Created by Stefan Bayer on 23.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize rootNavigationController;
@synthesize mainViewController;
@synthesize disclaimer;
@synthesize splash;

- (void)dealloc
{
    [_window release];
    [rootNavigationController release];
    [mainViewController release];
    [disclaimer release];
    [splash release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    self.mainViewController = [[[MainViewController alloc] init] autorelease];
    self.rootNavigationController = [[[UINavigationController alloc] initWithRootViewController:self.mainViewController] autorelease];
    self.rootNavigationController.navigationBar.hidden = YES;
    
    [self.window setRootViewController:self.rootNavigationController];
    
    self.disclaimer = [[[DisclaimerViewController alloc] init] autorelease];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"DISCLAIMER_ACCEPTED"]) {
        [self.rootNavigationController pushViewController:disclaimer animated:YES];
    }
    
    splash = [[SplashScreen alloc] init];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [splash splash];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SETTINGS_CHANGED" object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
