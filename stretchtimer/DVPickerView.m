//
//  DVPickerView.m
//  stretchtimer
//
//  Created by Alexander Schilling on 6/8/13.
//
//

#import "DVPickerView.h"

@implementation DVPickerView

#pragma mark -
#pragma mark Synthesizes

@synthesize delegate;

@synthesize backScrolls;
@synthesize highlightScrolls;
@synthesize actionScrolls;


#pragma mark -
#pragma mark Initializers

//init with this method
//point: top left point of the picker
//components: Array of Arrays of Strings for the row labels in the components
//widthMode: either equal or variable for equal component width or dynamic component width
-(id)initAtPoint:(CGPoint)point withComponents:(NSArray *)components widthMode:(DV_PICKER_WIDTH_MODE)widthMode {
    //we get the frame through the given point
    self = [super initWithFrame:CGRectMake(point.x, point.y, 320, 217)];
    if (self) {
        //these arrays hold the UIScrollViews for the unhighlighted background,
        //the highlighted foreground on the overlay and the top scroll for user interactions
        self.backScrolls = [[[NSMutableArray alloc] init] autorelease];
        self.highlightScrolls = [[[NSMutableArray alloc] init] autorelease];
        self.actionScrolls = [[[NSMutableArray alloc] init] autorelease];
        
        //number of components
        int c = [components count];
        
        //x is the x position of the component
        //y is the width of the component
        CGPoint componentFrames[c];
        
        //calculations for variable widths
        if (widthMode == DV_PICKER_WIDTH_MODE_VARIABLE) {
            //the widths of the widest rows in each component
            int componentWidths[c];
            //sum of all widths
            int widthSum = 0;
            //loop through all components
            for (int i=0; i<c; i++) {
                //min width
                componentWidths[i] = 0;
                //loop through all rows in the component and pick the widest
                for (NSString *string in [components objectAtIndex:i]) {
                    if ([string sizeWithFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24.0]].width > componentWidths[i]) {
                        componentWidths[i] = [string sizeWithFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24.0]].width;
                    }
                }
                //add to sum
                widthSum += componentWidths[i];
            }
            
            //calculate the frames
            
            //x position of the next component
            int currentX = 40;
            if (widthSum > 0)
            {
                for (int i=0; i<c; i++) {
                    //get the width of the component based on its part of the content width
                    int width = (DV_PICKER_CONTENT_WIDTH-20*c)*componentWidths[i]/widthSum + 20;
                    //create the frame
                    componentFrames[i] = CGPointMake(currentX, width);
                    //increase x position of next component by width of this one
                    currentX += width;
                }
            }
        }
        else { //widthMode == DV_PICKER_WIDTH_MODE_EQUAL
            if (c > 0)
            {
                for (int i=0; i<c; i++) {
                    //get the frames by dividing the width into equal pieces
                    componentFrames[i] = CGPointMake(DV_PICKER_CONTENT_WIDTH*i/c + 40, DV_PICKER_CONTENT_WIDTH/c);
                }
            }

        }
        
        //create all unhighlighted rows in the background
        for (int i=0; i<c; i++) {
            UIScrollView *backScroll = [[[UIScrollView alloc] initWithFrame:CGRectMake(componentFrames[i].x, 10, componentFrames[i].y, DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE*2)] autorelease];
            [backScroll setUserInteractionEnabled:NO];
            [backScroll setShowsHorizontalScrollIndicator:NO];
            [backScroll setShowsVerticalScrollIndicator:NO];
            [self addSubview:backScroll];
            [self.backScrolls addObject:backScroll];
            
            int rows = [[components objectAtIndex:i] count];
            [backScroll setContentSize:CGSizeMake(componentFrames[i].y, DV_PICKER_OVERLAY_HEIGHT*rows + DV_PICKER_OVERLAY_BORDER_SPACE*2)];
            for (int j=0; j<rows; j++) {
                UILabel *rowLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, j*DV_PICKER_OVERLAY_HEIGHT+8 + DV_PICKER_OVERLAY_BORDER_SPACE, componentFrames[i].y-20, 35)] autorelease];
                [rowLabel setText:[[components objectAtIndex:i] objectAtIndex:j]];
                [rowLabel setBackgroundColor:[UIColor clearColor]];
                [rowLabel setTextColor:DV_PICKER_TEXT_COLOR_NORMAL];
                [rowLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24.0]];
                [rowLabel setAdjustsFontSizeToFitWidth:YES];
                [backScroll addSubview:rowLabel];
            }
        }
        
        //highlight view (current element indicator)
        UIImageView *overlay = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 241, 43)] autorelease];
        [overlay setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
        [overlay setImage:[UIImage imageNamed:@"overlay.png"]];
        [self addSubview:overlay];
        
        //create the highlighted rows above the overlay and only in that area
        for (int i=0; i<c; i++) {
            UIScrollView *highlightScroll = [[[UIScrollView alloc] initWithFrame:CGRectMake(componentFrames[i].x, DV_PICKER_BORDER_WIDTH + DV_PICKER_OVERLAY_BORDER_SPACE, componentFrames[i].y, DV_PICKER_OVERLAY_HEIGHT)] autorelease];
            [highlightScroll setUserInteractionEnabled:NO];
            [highlightScroll setShowsHorizontalScrollIndicator:NO];
            [highlightScroll setShowsVerticalScrollIndicator:NO];
            [self addSubview:highlightScroll];
            [self.highlightScrolls addObject:highlightScroll];
            
            int rows = [[components objectAtIndex:i] count];
            [highlightScroll setContentSize:CGSizeMake(240/c, 43*rows)];
            for (int j=0; j<rows; j++) {
                UILabel *rowLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, j*DV_PICKER_OVERLAY_HEIGHT+8, componentFrames[i].y-20, 35)] autorelease];
                [rowLabel setText:[[components objectAtIndex:i] objectAtIndex:j]];
                [rowLabel setBackgroundColor:[UIColor clearColor]];
                [rowLabel setTextColor:DV_PICKER_TEXT_COLOR_HIGHLIGHTED];
                [rowLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24.0]];
                [rowLabel setAdjustsFontSizeToFitWidth:YES];
                [highlightScroll addSubview:rowLabel];
            }
        }
        
        UILabel *minsLabel = [[[UILabel alloc] initWithFrame:CGRectMake(overlay.frame.origin.x+50, self.frame.size.height/2-10, 60, 28)] autorelease];
        [minsLabel setText:NSLocalizedString(@"Minute_Key", @"")];
        [minsLabel setBackgroundColor:[UIColor clearColor]];
        [minsLabel setTextColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gradient.png"]]];
        [minsLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:16.0]];
        [self addSubview:minsLabel];
        
        UILabel *secsLabel = [[[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width/2+50, self.frame.size.height/2-10, 60, 28)] autorelease];
        [secsLabel setText:NSLocalizedString(@"Second_Key", @"")];
        [secsLabel setBackgroundColor:[UIColor clearColor]];
        [secsLabel setTextColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gradient.png"]]];
        [secsLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:16.0]];
        [self addSubview:secsLabel];
        
        //component separators
        for (int i=1; i<c; i++) {
            UIImageView *separator = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1, DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE*2)] autorelease];
            [separator setCenter:CGPointMake(componentFrames[i].x, self.frame.size.height/2)];
            [separator setImage:[UIImage imageNamed:@"separator.png"]];
            [self addSubview:separator];
        }
        
        //create the user interaction areas
        for (int i=0; i<c; i++) {
            UIScrollView *actionScroll = [[[UIScrollView alloc] initWithFrame:CGRectMake(componentFrames[i].x, 10, componentFrames[i].y, DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE*2)] autorelease];
            [actionScroll setShowsHorizontalScrollIndicator:NO];
            [actionScroll setShowsVerticalScrollIndicator:NO];
            [actionScroll setBounces:NO];
            [actionScroll setDelegate:self];
            [self addSubview:actionScroll];
            [self.actionScrolls addObject:actionScroll];
            
            //tap functionality (tap above overlay to go up and below to go down)
            UITapGestureRecognizer *recognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)] autorelease];
            [recognizer setNumberOfTouchesRequired:1];
            [recognizer setNumberOfTapsRequired:1];
            [actionScroll addGestureRecognizer:recognizer];
            
            int rows = [[components objectAtIndex:i] count];
            [actionScroll setContentSize:CGSizeMake(componentFrames[i].y, DV_PICKER_OVERLAY_HEIGHT*rows + DV_PICKER_OVERLAY_BORDER_SPACE*2)];
        }
        
        //fade functionality for non active rows
        for (int i=0; i<c; i++) {
            [self adjustFadesInComponent:i];
        }
    }
    return self;
}


#pragma mark -
#pragma mark Delegate and Recognizer Methods

//tap functionality (tap above overlay to go up and below to go down)
-(void)tapped:(UITapGestureRecognizer *)recognizer {
    //get y coordinate of the tap in its view
    int y = [recognizer locationInView:recognizer.view].y;
    //get which component was tapped
    int component = [actionScrolls indexOfObject:recognizer.view];
    //get active row of the tapped component
    int activeRow = [self getActiveRowForComponent:component];
    //get number of rows in the component
    int maxRows = [(UIScrollView *)[highlightScrolls objectAtIndex:component] contentSize].height / DV_PICKER_OVERLAY_HEIGHT;
    
    //if tapped above, check if it is not the first row, then animate to previous row
    if (y < activeRow*DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE && activeRow != 0) {
        [self selectRow:activeRow-1 inComponent:component animated:YES];
    }
    //if tapped below, check if it is not the last row, then animate to next row
    else if (y > (activeRow+1)*DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE && activeRow != maxRows) {
        [self selectRow:activeRow+1 inComponent:component animated:YES];
    }
    
    //send after animation
    [self performSelector:@selector(delayedDelegateSend) withObject:nil afterDelay:0.3];
}

//need to send update after animation done
-(void)delayedDelegateSend {
    for (int i=0; i<[self.backScrolls count]; i++)
        [delegate pickerView:self didSelectRow:[self getActiveRowForComponent:i] inComponent:i];
}

//sync scrolling of the user interaction with the other scroll views
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //get the component that scrolled, because all use the same delegate
    int component = [self.actionScrolls indexOfObject:scrollView];
    
    //get the back and highlight scrolls and sync scrolling status
    UIScrollView *backScroll = (UIScrollView *)[self.backScrolls objectAtIndex:component];
    [backScroll scrollRectToVisible:CGRectMake(0, scrollView.contentOffset.y, backScroll.frame.size.width, DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE*2) animated:NO];
    UIScrollView *highlightScroll = (UIScrollView *)[self.highlightScrolls objectAtIndex:component];
    [highlightScroll scrollRectToVisible:CGRectMake(0, scrollView.contentOffset.y, highlightScroll.frame.size.width, DV_PICKER_OVERLAY_HEIGHT) animated:NO];
    
    //adjust the fades
    [self adjustFadesInComponent:component];
}

//when dragging ended, we may need to scroll to active row
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    //if not decelerating, otherwise other delegate method will handle it
    if (!decelerate) {
        //scroll to the current active row
        [self adjustScrollViewToActive:scrollView];
    }
}

//when decelerating ended, scroll to active row
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self adjustScrollViewToActive:scrollView];
}


#pragma mark -
#pragma mark Adjuster Methods

//adjust the fades: the elements further from the center fade away
-(void)adjustFadesInComponent:(int)component {
    //get the scroll view
    UIScrollView *backScroll = (UIScrollView *)[self.backScrolls objectAtIndex:component];
    //the center of the scroll area
    int centerY = backScroll.contentOffset.y + DV_PICKER_OVERLAY_BORDER_SPACE + DV_PICKER_OVERLAY_HEIGHT/2;
    //loop through all rows
    for (int i=0; i<[backScroll.subviews count]; i++) {
        //extract the subview
        UIView *subview = [backScroll.subviews objectAtIndex:i];
        //get its y position
        int y = subview.center.y;
        //get the absolute difference
        int yDiff = abs(centerY-y);
        //standard is visible
        float alpha = 1.0;
        //while in the highlighted area, no fading is needed
        if (yDiff > DV_PICKER_OVERLAY_HEIGHT/2)
            //fade away from 1 (at the overlay) to 0 (at the border)
            alpha = -(yDiff-DV_PICKER_OVERLAY_HEIGHT/2)/(float)DV_PICKER_OVERLAY_BORDER_SPACE + 1;
        [subview setAlpha:alpha];
    }
}

//correct the scrolling to active row in case it stopped somewhere in between two rows
-(void)adjustScrollViewToActive:(UIScrollView *)scrollView {
    //get the component
    int component = [self.actionScrolls indexOfObject:scrollView];
    //get active row
    int activeRow = [self getActiveRowForComponent:component];
    
    //scroll to active row in all scroll views
    [self selectRow:activeRow inComponent:component animated:YES];
    
    //adjust fades afterwards
    [self adjustFadesInComponent:component];
    
    //send a select call to the delegate
    [delegate pickerView:self didSelectRow:activeRow inComponent:component];
}


#pragma mark -
#pragma mark Getters and Setters

//returns the index of the active row in the given component
-(int)getActiveRowForComponent:(int)component {
    //get the scroll view
    UIScrollView *scrollView = [self.actionScrolls objectAtIndex:component];
    //get the y offset
    int y = scrollView.contentOffset.y + DV_PICKER_OVERLAY_HEIGHT/2;
    //get active row
    int activeRow = y / DV_PICKER_OVERLAY_HEIGHT;
    return activeRow;
}

//select the specified row
-(void)selectRow:(int)row inComponent:(int)component animated:(BOOL)animated {
    //scroll all views to the new active row
    UIScrollView *backScroll = (UIScrollView *)[self.backScrolls objectAtIndex:component];
    [backScroll scrollRectToVisible:CGRectMake(0, DV_PICKER_OVERLAY_HEIGHT*row, backScroll.frame.size.width, DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE*2) animated:animated];
    UIScrollView *highlightScroll = (UIScrollView *)[self.highlightScrolls objectAtIndex:component];
    [highlightScroll scrollRectToVisible:CGRectMake(0, DV_PICKER_OVERLAY_HEIGHT*row, highlightScroll.frame.size.width, DV_PICKER_OVERLAY_HEIGHT) animated:animated];
    UIScrollView *actionScroll = (UIScrollView *)[self.actionScrolls objectAtIndex:component];
    [actionScroll scrollRectToVisible:CGRectMake(0, DV_PICKER_OVERLAY_HEIGHT*row, actionScroll.frame.size.width, DV_PICKER_OVERLAY_HEIGHT + DV_PICKER_OVERLAY_BORDER_SPACE*2) animated:animated];
}

@end
