//
//  DataManager.h
//  stretchtimer
//
//  Created by Stefan Bayer on 27.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

- (DataManager *)init;

- (void)savePauseTime:(int)pauseTime;
- (void)saveStretchTime:(int)stretchTime;
- (void)saveExerciseCount:(int)setsExerciseCount;
- (void)saveSoundStatus:(int)soundStatus;

- (int)getPauseTime;
- (int)getStretchTime;
- (int)getExerciseCount;
- (int)getSoundStatus;

@end
