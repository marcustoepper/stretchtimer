//
//  MainViewController.h
//  stretchtimer
//
//  Created by Stefan Bayer on 23.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusPanelViewController.h"
#import "ClockViewController.h"
#import "DataManager.h"
#import "SettingsViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface MainViewController : UIViewController
{
    StatusPanelViewController *statusPanel;
    ClockViewController *clockPanel;
    NSTimer *globalTimer;
    int timerCount;
    BOOL isStarted;
    BOOL isPaused;
    int nextPostion;
    int countdown;
    int exerciseCount;
    int setsCount;
    DataManager *dataManager;
    SettingsViewController *settingsViewController;
    UIButton *startStopButton;
    UIButton *resetButton;
    
    SystemSoundID holdSound;
    SystemSoundID changeSound;
    
    NSTimeInterval lastUpdate;
    float progress;
}

@property (strong, nonatomic) StatusPanelViewController *statusPanel;
@property (strong, nonatomic) ClockViewController *clockPanel;
@property (strong, nonatomic) NSTimer *globalTimer;
@property int timerCount;
@property BOOL isStarted;
@property int nextPosition;
@property int countdown;
@property int exerciseCount;
@property int setsCount;
@property (strong, nonatomic) DataManager *dataManager;
@property (strong, nonatomic) SettingsViewController *settingsViewController;
@property (strong, nonatomic) UIButton *startStopButton;
@property (strong, nonatomic) UIButton *resetButton;
@property BOOL isPaused;
@property NSTimeInterval lastUpdate;
@property float progress;

- (void)timerFireMethod:(NSTimer*)theTimer;
- (void)startStopAction:(id)sender;
- (void)resetPanelValues;
- (void)playPauseSound;
- (void)playStretchSound;

@end
