//
//  SplashScreen.m
//  stretchtimer
//
//  Created by Alexander Schilling on 5/25/13.
//
//

#import "SplashScreen.h"

@interface SplashScreen ()

@end

@implementation SplashScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        int SCREEN_HEIGHT = [[UIScreen mainScreen] bounds].size.height;
        
        [self.view setFrame:CGRectMake(0, 0, 320, SCREEN_HEIGHT)];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0, 0, 320, SCREEN_HEIGHT)];
        [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        if (SCREEN_HEIGHT > 480) //4 inch
            [backButton setImage:[UIImage imageNamed:@"splashscreen_high.png"] forState:UIControlStateNormal];
        else //3.5 inch
            [backButton setImage:[UIImage imageNamed:@"splashscreen.png"] forState:UIControlStateNormal];
        [backButton setAdjustsImageWhenHighlighted:NO];
        [self.view addSubview:backButton];
    }
    return self;
}

-(void)splash {
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.view];
    [self performSelector:@selector(backAction) withObject:nil afterDelay:2.0];
}

-(void)backAction {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self.view removeFromSuperview];
}

@end
