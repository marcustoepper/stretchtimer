//
//  ClockViewController.h
//  stretchtimer
//
//  Created by Stefan Bayer on 24.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockView.h"
#import "GradientLabel.h"

@interface ClockViewController : UIViewController
{
    ClockView *clockView;
    
    GradientLabel *minutes;
    GradientLabel *seconds;
    GradientLabel *doubleDots;
    
    int minutesToGo;
    int secondsToGo;
    
    int switches;
}

@property (strong, nonatomic) ClockView *clockView;

@property (strong, nonatomic) GradientLabel *minutes;
@property (strong, nonatomic) GradientLabel *seconds;
@property (strong, nonatomic) GradientLabel *doubleDots;

@property int minutesToGo;
@property int secondsToGo;

@property int switches;

- (int)setCountdown:(int)countdown;
- (void)resetClock;

@end
