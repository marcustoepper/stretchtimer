//
//  DataManager.m
//  stretchtimer
//
//  Created by Stefan Bayer on 27.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

- (DataManager *)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults]; 
        
        if (![defaults objectForKey:@"pauseTime"]) {
            [defaults setObject:[NSNumber numberWithInt:10] forKey:@"pauseTime"];
            [defaults setObject:[NSNumber numberWithInt:180] forKey:@"stretchTime"];
            [defaults setObject:[NSNumber numberWithInt:5] forKey:@"setsCount"];
            [defaults setObject:[NSNumber numberWithInt:1] forKey:@"soundEnabled"];
        }

        [defaults synchronize];

    }
    
    return self;
}

- (void)savePauseTime:(int)pauseTime
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:pauseTime] forKey:@"pauseTime"];
    [defaults synchronize];
}

- (void)saveStretchTime:(int)stretchTime
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:stretchTime] forKey:@"stretchTime"];
    [defaults synchronize];
}

- (void)saveExerciseCount:(int)exerciseCount
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:exerciseCount] forKey:@"setsCount"];
    [defaults synchronize];
}

- (void)saveSoundStatus:(int)soundStatus
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:soundStatus] forKey:@"soundEnabled"];
    [defaults synchronize];
}

- (int)getPauseTime
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"pauseTime"] intValue];
}

- (int)getStretchTime
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"stretchTime"] intValue];
}

- (int)getExerciseCount
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"setsCount"] intValue];
}

- (int)getSoundStatus
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"soundEnabled"] intValue];
}

@end
