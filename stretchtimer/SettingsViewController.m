//
//  SettingsViewController.m
//  stretchtimer
//
//  Created by Alexander Schilling on 5/4/13.
//
//

#import "SettingsViewController.h"
#import "Definitions.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

@synthesize settingsButton;

@synthesize stepsLabel;

@synthesize stretchTimeLabel;
@synthesize pauseTimeLabel;

@synthesize datePicker;

@synthesize dataManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.dataManager = [[[DataManager alloc] init] autorelease];
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:239.0/255.0 alpha:1.0]];
        
        self.settingsButton = [[[UIButton buttonWithType:UIButtonTypeCustom] retain] autorelease];
        [self.settingsButton setImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
        [self.settingsButton setBackgroundColor:[UIColor colorWithWhite:239.0/255.0 alpha:1.0]];
        [self.settingsButton setContentMode:UIViewContentModeCenter];
        [self.settingsButton setAdjustsImageWhenHighlighted:NO];
        self.settingsButton.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
        [self.settingsButton addTarget:self action:@selector(toSettingsAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.settingsButton];
        
        UIButton *impressumButton = [[[UIButton buttonWithType:UIButtonTypeCustom] retain] autorelease];
        [impressumButton setFrame:CGRectMake(270, 0, 50, 50)];
        [impressumButton setImage:[UIImage imageNamed:@"impressum.png"] forState:UIControlStateNormal];
        [impressumButton setContentMode:UIViewContentModeCenter];
        [impressumButton setAdjustsImageWhenHighlighted:NO];
        [impressumButton setBackgroundColor:[UIColor colorWithWhite:239.0/255.0 alpha:1.0]];
        [impressumButton addTarget:self action:@selector(toImpressumAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:impressumButton];
        
        UIView *firstLine = [[[UIView alloc] initWithFrame:CGRectMake(0, 50, SCREEN_WIDTH, 1)] autorelease];
        [firstLine setBackgroundColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.view addSubview:firstLine];
        
        UILabel *audioLabel = [[[UILabel alloc] initWithFrame:CGRectMake(35, 68, SCREEN_WIDTH/2, 30)] autorelease];
        [audioLabel setBackgroundColor:[UIColor clearColor]];
        [audioLabel setText:NSLocalizedString(@"Settings_Field1_Key", @"")];
        [audioLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12]];
        [audioLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.view addSubview:audioLabel];
        
        UIButton *audioButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [audioButton setFrame:CGRectMake(SCREEN_WIDTH-40-77, 66, 77, 27)];
        if ([self.dataManager getSoundStatus])
            [audioButton setImage:[UIImage imageNamed:@"switch_on.png"] forState:UIControlStateNormal];
        else
            [audioButton setImage:[UIImage imageNamed:@"switch_off.png"] forState:UIControlStateNormal];
        [audioButton addTarget:self action:@selector(toggleAudio:) forControlEvents:UIControlEventTouchUpInside];
        [audioButton setAdjustsImageWhenHighlighted:NO];
        [self.view addSubview:audioButton];
        
        UIView *secondLine = [[[UIView alloc] initWithFrame:CGRectMake(0, 110, SCREEN_WIDTH, 1)] autorelease];
        [secondLine setBackgroundColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.view addSubview:secondLine];
        
        UILabel *stepsTitleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(35, 128, SCREEN_WIDTH/2, 30)] autorelease];
        [stepsTitleLabel setBackgroundColor:[UIColor clearColor]];
        [stepsTitleLabel setText:NSLocalizedString(@"Settings_Field2_Key", @"")];
        [stepsTitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12]];
        [stepsTitleLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.view addSubview:stepsTitleLabel];
        
        self.stepsLabel = [[[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-40-28-80, 128, 80, 30)] autorelease];
        [self.stepsLabel setTextAlignment:NSTextAlignmentCenter];
        [self.stepsLabel setBackgroundColor:[UIColor clearColor]];
        [self.stepsLabel setText:[NSString stringWithFormat:@"%d",[self.dataManager getExerciseCount]]];
        [self.stepsLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12]];
        [self.stepsLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.view addSubview:self.stepsLabel];
        
        UIButton *minusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [minusButton setFrame:CGRectMake(SCREEN_WIDTH-40-28-80-28, 128, 28, 28)];
        [minusButton setImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
        [minusButton setAdjustsImageWhenHighlighted:NO];
        [minusButton addTarget:self action:@selector(decreaseSteps) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:minusButton];
        
        UIButton *plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [plusButton setFrame:CGRectMake(SCREEN_WIDTH-40-28, 128, 28, 28)];
        [plusButton setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
        [plusButton setAdjustsImageWhenHighlighted:NO];
        [plusButton addTarget:self action:@selector(increaseSteps) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:plusButton];
        
        UIView *thirdLine = [[[UIView alloc] initWithFrame:CGRectMake(0, 170, SCREEN_WIDTH, 1)] autorelease];
        [thirdLine setBackgroundColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.view addSubview:thirdLine];
        
        UIView *pickerZoich = [[[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-217-20-50 - (SCREEN_HEIGHT-480)/2, SCREEN_WIDTH, 226)] autorelease];
        [pickerZoich setUserInteractionEnabled:YES];
        [self.view addSubview:pickerZoich];
        
        UIButton *stretchTimeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [stretchTimeButton setFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, 50)];
        [stretchTimeButton addTarget:self action:@selector(setStretchTime) forControlEvents:UIControlEventTouchUpInside];
        [pickerZoich addSubview:stretchTimeButton];
        
        self.stretchTimeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(40, 10, SCREEN_WIDTH/2, 30)] autorelease];
        [self.stretchTimeLabel setText:NSLocalizedString(@"Settings_Field3_Key", @"")];
        [self.stretchTimeLabel setTextColor:[UIColor colorWithRed:97.0/255.0 green:172.0/255.0 blue:199.0/255.0 alpha:1.0]];
        [self.stretchTimeLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12]];
        [self.stretchTimeLabel setBackgroundColor:[UIColor clearColor]];
        [self.stretchTimeLabel setAdjustsFontSizeToFitWidth:YES];
        [stretchTimeButton addSubview:self.stretchTimeLabel];
        
        UIButton *pauseTimeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [pauseTimeButton setFrame:CGRectMake(SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, 50)];
        [pauseTimeButton addTarget:self action:@selector(setPauseTime) forControlEvents:UIControlEventTouchUpInside];
        [pickerZoich addSubview:pauseTimeButton];
        
        self.pauseTimeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(40, 10, SCREEN_WIDTH/2-80, 30)] autorelease];
        [self.pauseTimeLabel setText:NSLocalizedString(@"Settings_Field4_Key", @"")];
        [self.pauseTimeLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
        [self.pauseTimeLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:12]];
        [self.pauseTimeLabel setBackgroundColor:[UIColor clearColor]];
        [self.pauseTimeLabel setTextAlignment:NSTextAlignmentRight];
        [self.pauseTimeLabel setAdjustsFontSizeToFitWidth:YES];
        [pauseTimeButton addSubview:self.pauseTimeLabel];
        
        self.datePicker = [[[DVPickerView alloc] initAtPoint:CGPointMake(0, 50) withComponents:@[@[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"],@[@"0",@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55"]] widthMode:DV_PICKER_WIDTH_MODE_EQUAL] autorelease];
        [self.datePicker setDelegate:self];
        [self.datePicker selectRow:[self.dataManager getStretchTime]/60 inComponent:0 animated:NO];
        [self.datePicker selectRow:[self.dataManager getStretchTime]%60/5 inComponent:1 animated:NO];
        [pickerZoich addSubview:datePicker];
        
        currentMode = PICKER_STRETCH_MODE;
    }
    return self;
}

-(void)toggleAudio:(UIButton *)sender {
    if ([self.dataManager getSoundStatus]) {
        [sender setImage:[UIImage imageNamed:@"switch_off.png"] forState:UIControlStateNormal];
        [self.dataManager saveSoundStatus:SOUND_OFF];
    }
    else {
        [sender setImage:[UIImage imageNamed:@"switch_on.png"] forState:UIControlStateNormal];
        [self.dataManager saveSoundStatus:SOUND_ON];
    }
}

-(void)decreaseSteps {
    [self.dataManager saveExerciseCount:[self.dataManager getExerciseCount]-1];
    if ([self.dataManager getExerciseCount] < 1)
        [self.dataManager saveExerciseCount:1];
    [self.stepsLabel setText:[NSString stringWithFormat:@"%d",[self.dataManager getExerciseCount]]];
}

-(void)increaseSteps {
    [self.dataManager saveExerciseCount:[self.dataManager getExerciseCount]+1];
    [self.stepsLabel setText:[NSString stringWithFormat:@"%d",[self.dataManager getExerciseCount]]];
}

-(void)setStretchTime {
    currentMode = PICKER_STRETCH_MODE;
    [self.datePicker selectRow:[self.dataManager getStretchTime]/60 inComponent:0 animated:YES];
    [self.datePicker selectRow:[self.dataManager getStretchTime]%60/5 inComponent:1 animated:YES];
    [self.stretchTimeLabel setTextColor:[UIColor colorWithRed:97.0/255.0 green:172.0/255.0 blue:199.0/255.0 alpha:1.0]];
    [self.pauseTimeLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
}

-(void)setPauseTime {
    currentMode = PICKER_PAUSE_MODE;
    [self.datePicker selectRow:[self.dataManager getPauseTime]/60 inComponent:0 animated:YES];
    [self.datePicker selectRow:[self.dataManager getPauseTime]%60/5 inComponent:1 animated:YES];
    [self.pauseTimeLabel setTextColor:[UIColor colorWithRed:97.0/255.0 green:172.0/255.0 blue:199.0/255.0 alpha:1.0]];
    [self.stretchTimeLabel setTextColor:[UIColor colorWithWhite:174.0/255.0 alpha:1.0]];
}

-(void)toSettingsAction {
    [UIView beginAnimations:nil context:nil];
    
    for (UIView *view in self.view.subviews) {
        [view setFrame:CGRectOffset(view.frame, 0, SCREEN_TOP_CORRECTION)];
    }
    
    [self.view setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT+SCREEN_TOP_CORRECTION)];
    
    [UIView commitAnimations];
    
    [self.settingsButton setImage:[UIImage imageNamed:@"settings_tick.png"] forState:UIControlStateNormal];
    [self.settingsButton removeTarget:self action:@selector(toSettingsAction) forControlEvents:UIControlEventTouchUpInside];
    [self.settingsButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SETTINGS_CHANGED" object:nil];
}

-(void)backAction {
    [UIView beginAnimations:nil context:nil];
    
    for (UIView *view in self.view.subviews) {
        [view setFrame:CGRectOffset(view.frame, 0, -SCREEN_TOP_CORRECTION)];
    }
    
    [self.view setFrame:CGRectMake(0, SCREEN_HEIGHT-50, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    [UIView commitAnimations];
    
    [self.settingsButton setImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
    [self.settingsButton removeTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.settingsButton addTarget:self action:@selector(toSettingsAction) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SETTINGS_CHANGED" object:nil];
}

-(void)toImpressumAction {
    ImprintViewController *imprint = [[[ImprintViewController alloc] init] autorelease];
    [(UINavigationController *)[[UIApplication sharedApplication] keyWindow].rootViewController pushViewController:imprint animated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PAUSE" object:nil];
}

-(void)pickerView:(DVPickerView *)pickerView didSelectRow:(int)row inComponent:(int)component {
    if (currentMode == PICKER_STRETCH_MODE) {
        if (component == 0) {
            int time = (int)[self.dataManager getStretchTime]%60+row*60;
            if (time == 0) {
                time = 5;
                [pickerView selectRow:1 inComponent:1 animated:YES];
            }
            [self.dataManager saveStretchTime:time];
        }
        else {
            int time = (int)[self.dataManager getStretchTime]-[self.dataManager getStretchTime]%60+row*5;
            if (time == 0) {
                time = 5;
                [pickerView selectRow:1 inComponent:1 animated:YES];
            }
            [self.dataManager saveStretchTime:time];
        }
    }
    else {
        if (component == 0) {
            int time = (int)[self.dataManager getPauseTime]%60+row*60;
            if (time == 0) {
                time = 5;
                [pickerView selectRow:1 inComponent:1 animated:YES];
            }
            [self.dataManager savePauseTime:time];
        }
        else {
            int time = (int)[self.dataManager getPauseTime]-[self.dataManager getPauseTime]%60+row*5;
            if (time == 0) {
                time = 5;
                [pickerView selectRow:1 inComponent:1 animated:YES];
            }
            [self.dataManager savePauseTime:time];
        }
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
