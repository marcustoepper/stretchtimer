//
//  ClockView.m
//  stretchtimer
//
//  Created by Alexander Schilling on 5/3/13.
//
//

#import "ClockView.h"
#import "Definitions.h"

@implementation ClockView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        progress = 0.0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update:) name:@"UPDATE" object:nil];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath addArcWithCenter:CGPointMake(SCREEN_WIDTH/2, rect.size.height/2+9) radius:89 startAngle:M_PI*3/2 endAngle:M_PI*3/2+progress*M_PI*2 clockwise:YES];
    [bezierPath setLineWidth:18];
    [[UIColor colorWithRed:219.0/255.0 green:236.0/255.0 blue:242.0/255.0 alpha:1.0] setStroke];
    [bezierPath stroke];
    
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:97.0/255.0 green:172.0/255.0 blue:199.0/255.0 alpha:1.0].CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(SCREEN_WIDTH/2-80, rect.size.height/2-71, 160, 160));
}

-(void)update:(NSNotification *)notification {
    progress = [[[notification userInfo] objectForKey:@"progress"] floatValue];
    [self setNeedsDisplay];
}

@end
