//
//  MainViewController.m
//  stretchtimer
//
//  Created by Stefan Bayer on 23.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"
#import "Definitions.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize statusPanel;
@synthesize clockPanel;
@synthesize globalTimer;
@synthesize timerCount;
@synthesize isStarted;
@synthesize nextPosition;
@synthesize countdown;
@synthesize exerciseCount;
@synthesize setsCount;
@synthesize dataManager;
@synthesize settingsViewController;
@synthesize startStopButton;
@synthesize resetButton;
@synthesize isPaused;
@synthesize lastUpdate;
@synthesize progress;

-(void)dealloc {
    [self.globalTimer invalidate];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isPaused = NO;
        
        self.dataManager = [[[DataManager alloc] init] autorelease];
        
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        NSString *path  = [[NSBundle mainBundle] pathForResource:@"hold" ofType:@"caf"];
        //NSURL *pathURL = [NSURL fileURLWithPath: path];
        NSURL *pathURL = [[[NSURL alloc] initFileURLWithPath:path] autorelease];

        AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain(pathURL), &holdSound);
        
        path  = [[NSBundle mainBundle] pathForResource:@"change" ofType:@"caf"];
        //pathURL = [NSURL fileURLWithPath: path];
        //pathURL = nil;
        pathURL = [[[NSURL alloc] initFileURLWithPath:path] autorelease];

        AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain(pathURL), &changeSound);
        
        self.statusPanel = [[[StatusPanelViewController alloc] init] autorelease];
        self.statusPanel.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        [self.view addSubview:self.statusPanel.view];
        
        self.startStopButton = [[[UIButton buttonWithType:UIButtonTypeCustom] retain] autorelease];
        self.startStopButton.frame = CGRectMake(0, 0, 45, 45);
        [self.startStopButton setCenter:CGPointMake(SCREEN_WIDTH*0.2, 0.57*SCREEN_HEIGHT)];
        [self.startStopButton setAdjustsImageWhenHighlighted:NO];
        [self.startStopButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        [self.startStopButton addTarget:self action:@selector(startStopAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.startStopButton];
        
        self.resetButton = [[[UIButton buttonWithType:UIButtonTypeCustom] retain] autorelease];
        self.resetButton.frame = CGRectMake(0, 0, 45, 45);
        [self.resetButton setCenter:CGPointMake(SCREEN_WIDTH*0.8, 0.57*SCREEN_HEIGHT)];
        [self.resetButton setAdjustsImageWhenHighlighted:NO];
        [self.resetButton setImage:[UIImage imageNamed:@"stop.png"] forState:UIControlStateNormal];
        [self.resetButton addTarget:self action:@selector(resetPanelValues) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.resetButton];
        
        self.clockPanel = [[[ClockViewController alloc] init] autorelease];
        self.clockPanel.view.frame = CGRectMake(0, 0, 320, 0.45*SCREEN_HEIGHT);
        [self.view addSubview:self.clockPanel.view];
        
        SettingsViewController *settings = [[SettingsViewController alloc] init];
        [settings.view setFrame:CGRectMake(0, SCREEN_HEIGHT-50, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [self.view addSubview:settings.view];
        
        self.globalTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/25.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetPanelValues) name:@"SETTINGS_CHANGED" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseAction) name:@"PAUSE" object:nil];
        
        [self resetPanelValues];
    }
    return self;
}

- (void)playPauseSound
{
    AudioServicesPlaySystemSound(holdSound);
}

- (void)playStretchSound
{
    AudioServicesPlaySystemSound(changeSound);
}

- (void)resetPanelValues
{
    self.isStarted = NO;
    self.isPaused = NO;
    self.nextPosition = STRETCH;
    self.timerCount = 0;
    self.progress = 0.0;
    self.countdown = 10;
    self.exerciseCount = 0;
    self.setsCount = 0;
    [self.clockPanel setCountdown:self.countdown];
    [self.clockPanel resetClock];
    [self.statusPanel reset];
    [self.startStopButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
}

- (void)timerFireMethod:(NSTimer*)theTimer
{
    if (!self.isPaused && self.isStarted) {
        self.progress += ([[NSDate date] timeIntervalSince1970]-self.lastUpdate) / self.countdown;
    }
    self.lastUpdate = [[NSDate date] timeIntervalSince1970];
    
    self.timerCount = self.progress*self.countdown;
    
    int switches = [self.clockPanel setCountdown:self.countdown - self.timerCount];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATE" object:self userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:progress] forKey:@"progress"]];
    
    if (switches != -1) {
        if (self.nextPosition == PAUSE) {
            if ([self.dataManager getSoundStatus] == SOUND_ON) {
               [self playPauseSound];
            }
            self.nextPosition = [self.statusPanel transformPositionPanelToStatus:PAUSE];
            self.exerciseCount++;
            [self.statusPanel transformExercisePanelWithExerciseCount:self.exerciseCount];
            
            if (self.exerciseCount == [self.dataManager getExerciseCount]) {
                self.setsCount++;
                [self.statusPanel transformSetsPanelWithSetsCount:self.setsCount];
                self.exerciseCount = 0;
            }
        } else {
            if ([self.dataManager getSoundStatus] == SOUND_ON) {
                [self playStretchSound];
            }
            self.nextPosition = [self.statusPanel transformPositionPanelToStatus:STRETCH];
        }
        
        self.timerCount = 0;
        self.progress = 0.0;
        
        if (switches %2 == 0) {
            self.countdown = [self.dataManager getPauseTime];
        } else {
            self.countdown = [self.dataManager getStretchTime];
        }
    }
}

- (void)startStopAction:(id)sender
{
    if (self.isStarted) {
        if (self.isPaused == NO) {
            [self.startStopButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
            self.isPaused = YES;
        } else {
            self.isPaused = NO;
            [self.startStopButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        }
    } else {
        if ([self.dataManager getSoundStatus] == SOUND_ON) {
            [self playPauseSound];
        }
        
        [self.startStopButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        
        [self.clockPanel setCountdown:self.countdown];
        
        self.isStarted = YES;
        self.isPaused = NO;
    }
}

-(void)pauseAction {
    if (self.isStarted && !self.isPaused) {
        [self.startStopButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        self.isPaused = YES;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
