//
//  GradientLabel.m
//  stretchtimer
//
//  Created by Alexander Schilling on 5/2/13.
//
//

#import "GradientLabel.h"

@implementation GradientLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setTextColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gradient.png"]]];
        [self setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:frame.size.height-6]];
        [self setTextAlignment:NSTextAlignmentCenter];
        [self setShadowOffset:CGSizeMake(1, 1)];
        [self setShadowColor:[UIColor colorWithWhite:0.2 alpha:0.1]];
    }
    return self;
}

@end
