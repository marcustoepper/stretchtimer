//
//  Definitions.h
//  stretchtimer
//
//  Created by Stefan Bayer on 24.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PAUSE 0
#define STRETCH 1
#define PICKER_STRETCH_MODE 0
#define PICKER_PAUSE_MODE 1
#define SOUND_ON 1
#define SOUND_OFF 0

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT (([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) ? [[UIScreen mainScreen] bounds].size.height : ([[UIScreen mainScreen] bounds].size.height-20))
#define SCREEN_TOP_CORRECTION (([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) ? 20 : 0)

@protocol Definitions <NSObject>

@end
