//
//  ClockViewController.m
//  stretchtimer
//
//  Created by Stefan Bayer on 24.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ClockViewController.h"
#import "Definitions.h"

@interface ClockViewController ()

@end

@implementation ClockViewController

@synthesize clockView;

@synthesize minutes;
@synthesize seconds;
@synthesize doubleDots;

@synthesize minutesToGo;
@synthesize secondsToGo;

@synthesize switches;

- (void)dealloc
{
    [clockView release];
    
    [minutes release];
    [seconds release];
    [doubleDots release];
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor clearColor]];
        
        self.clockView = [[[ClockView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.45*SCREEN_HEIGHT)] autorelease];
        [self.view addSubview:clockView];
        
        self.minutes = [[[GradientLabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-53, 0.45*SCREEN_HEIGHT/2-9, 50, 50)] autorelease];
        self.minutes.textAlignment = NSTextAlignmentRight;
        self.minutes.font = [UIFont fontWithName:@"MyriadPro-Bold" size:38];
        self.minutes.text = @"00";
        [self.view addSubview:self.minutes];
        
        self.doubleDots = [[[GradientLabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-5, 0.45*SCREEN_HEIGHT/2-9, 10, 50)] autorelease];
        self.doubleDots.font = [UIFont fontWithName:@"MyriadPro-Bold" size:38];
        self.doubleDots.text = @":";
        [self.view addSubview:self.doubleDots];
        
        self.seconds = [[[GradientLabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2+5, 0.45*SCREEN_HEIGHT/2-9, 50, 50)] autorelease];
        self.seconds.textAlignment = NSTextAlignmentLeft;
        self.seconds.font = [UIFont fontWithName:@"MyriadPro-Bold" size:38];
        self.seconds.text = @"00";     
        [self.view addSubview:self.seconds];
        
        GradientLabel *minLabel = [[[GradientLabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-47, 0.45*SCREEN_HEIGHT/2+25, 45, 35)] autorelease];
        minLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12];
        minLabel.text = NSLocalizedString(@"Minute_Key", nil);
        [self.view addSubview:minLabel];
        
        GradientLabel *secLabel = [[[GradientLabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2+5, 0.45*SCREEN_HEIGHT/2+25, 45, 35)] autorelease];
        secLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12];
        secLabel.text = NSLocalizedString(@"Second_Key", nil);
        [self.view addSubview:secLabel];
        
        [self resetClock];
        
    }
    return self;
}

- (void)resetClock
{
    self.switches = 0;
}

- (int)setCountdown:(int)countdown
{
    self.minutesToGo = countdown / 60;
    if (self.minutesToGo < 10)
        self.minutes.text = [NSString stringWithFormat:@"0%i", self.minutesToGo];
    else
        self.minutes.text = [NSString stringWithFormat:@"%i", self.minutesToGo];
    self.secondsToGo = countdown - (self.minutesToGo * 60);
    if (self.secondsToGo < 10)
        self.seconds.text = [NSString stringWithFormat:@"0%i", self.secondsToGo];
    else
        self.seconds.text = [NSString stringWithFormat:@"%i", self.secondsToGo];
    
    if (countdown == 0) {
        self.switches++;
        return self.switches;
    }
    
    // no switch
    return -1;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
