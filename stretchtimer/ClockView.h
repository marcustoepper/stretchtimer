//
//  ClockView.h
//  stretchtimer
//
//  Created by Alexander Schilling on 5/3/13.
//
//

#import <UIKit/UIKit.h>

@interface ClockView : UIView {
    float progress;
}

-(void)update:(NSNotification *)notification;

@end
