//
//  PositionView.h
//  stretchtimer
//
//  Created by Alexander Schilling on 5/3/13.
//
//

#import <UIKit/UIKit.h>

@interface PositionView : UIView {
    NSTimeInterval animationStarted;
    BOOL animating;
}

-(void)animate;

@end
